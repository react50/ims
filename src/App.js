import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import CreateInvoice from "./Components/CreateInvoice";
import InvoicesList from "./Components/InvoicesList";
import EditInvoice from "./Components/EditInvoice";
import "./App.css";

function App() {
  return (
    <div className="App">
      {/* <Router>
        <div className="container">
          <nav className="navbar navbar-expand-lg navheader bg-dark mt-1">
            <div className="collapse navbar-collapse">
              <ul className="navbar-nav mr-auto">
                <li className="nav-item">
                  <Link to={"/CreateInvoice"} className="nav-link">
                    Add Invoice
                  </Link>
                </li>

                <li className="nav-item">
                  <Link to={"/InvoicesList"} className="nav-link">
                    Invoices List
                  </Link>
                </li>
              </ul>
            </div>
          </nav>{" "}
          <br />
          <Switch>
            <Route exact path="/CreateInvoice" component={CreateInvoice} />

            <Route path="/edit/:id" component={EditInvoice} />

            <Route exact path="/InvoicesList" component={InvoicesList} />
          </Switch>
        </div>
      </Router> */}

      <Router>
        <div>
          <nav className="navbar navbar-expand navbar-dark bg-dark">
            <a href="/" className="navbar-brand">
              Invoice Management System
            </a>

            <div className="navbar-nav mr-auto">
              <li className="nav-item">
                <Link to={"/invoices"} className="nav-link">
                  Invoices
                </Link>
              </li>
              <li className="nav-item">
                <Link to={"/add"} className="nav-link">
                  Add
                </Link>
              </li>
            </div>
          </nav>

          <div className="container mt-3">
            <Switch>
              <Route exact path={["/", "/invoices"]} component={InvoicesList} />
              <Route exact path="/add" component={CreateInvoice} />
              <Route path="/invoices/:id" component={EditInvoice} />
            </Switch>
          </div>
        </div>
      </Router>
    </div>
  );
}

export default App;
