import axios from "axios";

export default axios.create({
  baseURL: "https://protected-chamber-82435.herokuapp.com/",
  headers: {
    "Content-type": "application/json",
  },
});
