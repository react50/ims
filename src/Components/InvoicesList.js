import React, { useState, useEffect, useMemo } from "react";
import {
  Container,
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  Table,
} from "reactstrap";
import InvoiceDataService from "../Services/InvoiceService";
import "../styles.css";

const useSortableData = (items, config = null) => {
  const [sortConfig, setSortConfig] = useState(config);

  const sortedItems = useMemo(() => {
    let sortableItems = [...items];
    if (sortConfig !== null) {
      sortableItems.sort((a, b) => {
        console.log(sortConfig.key);
        if (sortConfig.key === "date") {
          a = a.date.split("/").reverse().join("");
          b = b.date.split("/").reverse().join("");
          if (a < b) {
            return sortConfig.direction === "ascending" ? -1 : 1;
          }
          if (a > b) {
            return sortConfig.direction === "ascending" ? 1 : -1;
          }
          return 0;
        } else {
          if (a[sortConfig.key] < b[sortConfig.key]) {
            return sortConfig.direction === "ascending" ? -1 : 1;
          }
          if (a[sortConfig.key] > b[sortConfig.key]) {
            return sortConfig.direction === "ascending" ? 1 : -1;
          }
          return 0;
        }
      });
    }
    return sortableItems;
  }, [items, sortConfig]);

  const requestSort = (key) => {
    let direction = "ascending";
    if (
      sortConfig &&
      sortConfig.key === key &&
      sortConfig.direction === "ascending"
    ) {
      direction = "descending";
    }
    setSortConfig({ key, direction });
  };

  return { items: sortedItems, requestSort, sortConfig };
};

const InvoicesList = (props) => {
  const [invoices, setInvoices] = useState([]);
  const [searchTerm, setSearchTerm] = useState("");
  const { items, requestSort, sortConfig } = useSortableData(invoices);

  const getClassNamesFor = (name) => {
    if (!sortConfig) {
      return;
    }
    return sortConfig.key === name ? sortConfig.direction : undefined;
  };

  useEffect(() => {
    retriveInvoices(searchTerm);
  }, [searchTerm, sortConfig]);

  const retriveInvoices = (searchValue) => {
    InvoiceDataService.getAll()
      .then((response) => {
        const results = response.data.filter((invoice) =>
          invoice.billTo.companyName
            .toLowerCase()
            .includes(searchValue.toLowerCase())
        );
        setInvoices(results);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const performeSearch = (event) => {
    setSearchTerm(event.target.value);
  };

  const deleteInvoice = (id) => {
    InvoiceDataService.remove(id)
      .then((result) => {
        console.log(result);
        retriveInvoices();
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const editInvoice = (id) => {
    props.history.push({
      pathname: "/invoices/" + id,
    });
  };

  return (
    <Container className="animated fadeIn">
      <Row>
        <Col>
          <Card>
            <CardHeader>Invoices List</CardHeader>
            <CardBody>
              <input
                className="form-control mb-2"
                type="text"
                placeholder="Search by Company Name"
                value={searchTerm}
                onChange={performeSearch}
              />
              {invoices.length !== 0 ? (
                <Table hover bordered striped responsive size="sm">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>
                        <button
                          type="button"
                          onClick={() => requestSort("date")}
                          className={getClassNamesFor("date")}
                        >
                          Date
                        </button>
                      </th>
                      <th>Compan Name</th>
                      <th>Street Address</th>
                      <th>City</th>
                      <th>State</th>
                      <th>Email</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    {items.map((item, idx) => {
                      return (
                        <tr key={idx}>
                          <td>{item.id}</td>
                          <td>{item.date}</td>
                          <td>{item.billTo.companyName}</td>
                          <td>{item.billTo.streetAddress}</td>
                          <td>{item.billTo.city}</td>
                          <td>{item.billTo.state}</td>
                          <td>{item.billTo.email}</td>
                          <td className="btn-group">
                            <button
                              className="btn btn-success"
                              onClick={() => {
                                editInvoice(item.id);
                              }}
                            >
                              Edit
                            </button>

                            <button
                              className="btn btn-danger"
                              onClick={() => {
                                deleteInvoice(item.id);
                              }}
                            >
                              Delete
                            </button>
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </Table>
              ) : (
                <Table hover bordered striped responsive size="sm">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Date</th>
                      <th>Company Name</th>
                      <th>Street Address</th>
                      <th>City</th>
                      <th>State</th>
                      <th>Email</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td colSpan={8}>No data to display</td>
                    </tr>
                  </tbody>
                </Table>
              )}
            </CardBody>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};

export default InvoicesList;
