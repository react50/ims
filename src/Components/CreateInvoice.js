import React, { useState } from "react";
import InvoiceDataService from "../Services/InvoiceService";
import {
  Button,
  Card,
  CardBody,
  Col,
  Container,
  Form,
  Input,
  Label,
  FormGroup,
  Table,
  Spinner,
  Row,
} from "reactstrap";

function CreateInvoice(props) {
  const initialInvoiceState = {
    id: "",
    date: "",
    billTo: {
      customerId: "",
      terms: "",
      name: "",
      companyName: "",
      streetAddress: "",
      city: "",
      state: "",
      ZIP: "",
      phone: "",
      email: "",
    },
    invoiceData: [
      {
        description: "",
        qty: "",
        unitPrice: "",
        amount: "",
      },
    ],
  };

  const [invoice, setInvoice] = useState(initialInvoiceState);

  const InsertInvoice = (e) => {
    e.preventDefault();
    InvoiceDataService.create(invoice)
      .then((response) => {
        console.log(response.data);
        props.history.push("/");
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const handleInputChange = (e) => {
    e.persist();
    const { name, value } = e.target;
    setInvoice({
      ...invoice,
      billTo: { ...invoice.billTo, [name]: value },
      [name]: value,
    });
  };

  const handleChange = (e, index) => {
    e.persist();
    const { name, value } = e.target;
    const newInvoiceData = invoice.invoiceData;
    newInvoiceData[index][name] = value;
    setInvoice({
      ...invoice,
      invoiceData: newInvoiceData,
    });
  };

  const addNewItem = () => {
    const newInvoiceData = invoice.invoiceData;
    newInvoiceData[invoice.invoiceData.length] = {
      description: "",
      qty: "",
      unitPrice: "",
      amount: "",
    };
    setInvoice({
      ...invoice,
      invoiceData: newInvoiceData,
    });
  };

  const removeItem = (e, index) => {
    var items = [...invoice.invoiceData]; // make a separate copy of the array
    //var index = items.indexOf(e.target.value);
    if (index !== -1) {
      items.splice(index, 1);
      setInvoice({
        ...invoice,
        invoiceData: items,
      });
    }
  };

  return (
    <div>
      {invoice.id !== 0 ? (
        <Container className="app flex-row align-items-center">
          <Row className="justify-content-center">
            <Col md="12" lg="10" xl="8">
              <Card className="mx-4">
                <CardBody className="p-4">
                  <Form onSubmit={InsertInvoice}>
                    <Row form>
                      <Col md={6}>
                        <FormGroup className="text-left mb-3">
                          <Label for="date">Date</Label>
                          <Input
                            type="text"
                            name="date"
                            id="date"
                            className="form-control"
                            placeholder="Date"
                            value={invoice.date}
                            onChange={handleInputChange}
                          />
                        </FormGroup>
                      </Col>
                      <Col md={6}>
                        <FormGroup className="text-left mb-3">
                          <Label for="id">Terms</Label>
                          <Input
                            type="text"
                            name="terms"
                            id="terms"
                            className="form-control"
                            placeholder="Terms"
                            value={invoice.billTo.terms}
                            onChange={handleInputChange}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row form>
                      <Col md={6}>
                        <FormGroup className="text-left mb-3">
                          <Label for="companyName">Company Name</Label>
                          <Input
                            type="text"
                            placeholder="Company Name"
                            name="companyName"
                            id="companyName"
                            className="form-control"
                            value={invoice.billTo.companyName}
                            onChange={handleInputChange}
                          />
                        </FormGroup>
                      </Col>
                      <Col md={6}>
                        <FormGroup className="text-left mb-3">
                          <Label for="streetAddress">Street Address</Label>
                          <Input
                            type="text"
                            placeholder="Street Address"
                            name="streetAddress"
                            id="streetAddress"
                            className="form-control"
                            value={invoice.billTo.streetAddress}
                            onChange={handleInputChange}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row form>
                      <Col md={6}>
                        <FormGroup className="text-left mb-3">
                          <Label for="state">State</Label>
                          <Input
                            type="text"
                            placeholder="State"
                            name="state"
                            id="state"
                            className="form-control"
                            value={invoice.billTo.state}
                            onChange={handleInputChange}
                          />
                        </FormGroup>
                      </Col>
                      <Col md={6}>
                        <FormGroup className="text-left mb-3">
                          <Label for="city">City</Label>
                          <Input
                            type="text"
                            placeholder="City"
                            name="city"
                            id="city"
                            className="form-control"
                            value={invoice.billTo.city}
                            onChange={handleInputChange}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row form>
                      <Col md={3}>
                        <FormGroup className="text-left mb-3">
                          <Label for="state">ZIP</Label>
                          <Input
                            type="text"
                            placeholder="ZIP"
                            name="ZIP"
                            id="ZIP"
                            className="form-control"
                            value={invoice.billTo.ZIP}
                            onChange={handleInputChange}
                          />
                        </FormGroup>
                      </Col>
                      <Col md={3}>
                        <FormGroup className="text-left mb-3">
                          <Label for="state">Phone</Label>
                          <Input
                            type="text"
                            placeholder="Phone"
                            name="phone"
                            id="phone"
                            className="form-control"
                            value={invoice.billTo.phone}
                            onChange={handleInputChange}
                          />
                        </FormGroup>
                      </Col>
                      <Col md={6}>
                        <FormGroup className="text-left mb-3">
                          <Label for="state">Email</Label>
                          <Input
                            type="text"
                            placeholder="Email"
                            name="email"
                            id="email"
                            className="form-control"
                            value={invoice.billTo.email}
                            onChange={handleInputChange}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Table hover bordered responsive size="sm">
                      <thead>
                        <tr className="d-flex">
                          <th className="col-4">Item Description</th>
                          <th className="col-2">Unit Price</th>
                          <th className="col-2">Quantity</th>
                          <th className="col-2">Amount</th>
                          <th className="col-2">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        {invoice.invoiceData.map((item, idx) => {
                          return (
                            <tr key={idx} className="d-flex">
                              <td className="col-4">
                                <Input
                                  type="text"
                                  name="description"
                                  id="description"
                                  className="form-control"
                                  value={item.description}
                                  onChange={(e) => handleChange(e, idx)}
                                />
                              </td>
                              <td className="col-2">
                                <Input
                                  type="text"
                                  name="unitPrice"
                                  id="unitPrice"
                                  className="form-control"
                                  value={item.unitPrice}
                                  onChange={(e) => handleChange(e, idx)}
                                />
                              </td>
                              <td className="col-2">
                                <Input
                                  type="text"
                                  name="qty"
                                  id="qty"
                                  className="form-control"
                                  value={item.qty}
                                  onChange={(e) => handleChange(e, idx)}
                                />
                              </td>
                              <td className="col-2">
                                <Input
                                  type="amount"
                                  name="amount"
                                  id="amount"
                                  className="form-control"
                                  value={item.amount}
                                  onChange={(e) => handleChange(e, idx)}
                                />
                              </td>
                              <td className="col-2">
                                <Button
                                  className="btn btn-danger"
                                  onClick={(e) => removeItem(e, idx)}
                                >
                                  <span>Remove</span>
                                </Button>
                              </td>
                            </tr>
                          );
                        })}
                      </tbody>
                    </Table>
                    <Row>
                      <Col className="mb-2 d-flex">
                        <Button className="btn btn-info" onClick={addNewItem}>
                          <span>Add new item</span>
                        </Button>
                      </Col>
                    </Row>
                    <Row>
                      <Col md={6}>
                        <Button
                          type="submit"
                          className="btn btn-success mb-1"
                          block
                        >
                          <span>Save</span>
                        </Button>
                      </Col>
                      <Col md={6}>
                        <Button
                          className="btn mb-1"
                          block
                          onClick={() => {
                            props.history.push("/invoices");
                          }}
                        >
                          <span>Cancel</span>
                        </Button>
                      </Col>
                    </Row>
                  </Form>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      ) : (
        <Container>
          <Row className="justify-content-center">
            <Col md="12" lg="10" xl="8">
              <Card className="mx-4">
                <CardBody className="p-4">
                  <Spinner color="primary" />
                </CardBody>
              </Card>
            </Col>
          </Row>
        </Container>
      )}
    </div>
  );
}
export default CreateInvoice;
